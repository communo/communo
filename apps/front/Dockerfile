FROM node:20-alpine AS base

# Common dependencies
RUN apk update && apk add --no-cache libc6-compat

WORKDIR /app

# Install Turbo globally
RUN yarn global add turbo

# Dependencies stage
FROM base AS dependencies
COPY . ./
RUN yarn install

# Build stage for production
FROM dependencies AS builder
RUN turbo prune --scope=front --docker
COPY . .
RUN yarn turbo build --filter=front...

# Production stage
FROM base AS production
WORKDIR /app
COPY --from=builder /app/apps/front/next.config.js ./
COPY --from=builder /app/apps/front/package.json ./
COPY --from=builder /app/apps/front/.next/standalone ./
COPY --from=builder /app/apps/front/.next/static ./apps/front/.next/static
COPY --from=builder /app/apps/front/public ./apps/front/public
CMD ["node", "apps/front/server.js"]

# Development stage
FROM dependencies AS development
WORKDIR /app
COPY . .
CMD ["yarn", "dev"]
