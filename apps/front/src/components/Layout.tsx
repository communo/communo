import { ReactNode } from 'react'
import { Box, Flex, Link, Stack } from '@chakra-ui/react'
import NextLink from 'next/link'

interface LayoutProps {
    children: ReactNode
}

const Layout = ({ children }: LayoutProps) => {
    return (
        <Flex direction="column" minHeight="100vh">
            <Box as="nav" bg="teal.400" color="white" padding="4">
                <Flex
                    justify="space-between"
                    align="center"
                    maxWidth="1200px"
                    mx="auto"
                >
                    <Box>
                        <NextLink href="/" passHref>
                            LOGO
                        </NextLink>
                    </Box>
                    <Stack direction="row" spacing="4">
                        <NextLink href="/about" passHref>
                            About
                        </NextLink>
                        <NextLink href="/projects" passHref>
                            Projects
                        </NextLink>
                        <NextLink href="/contact" passHref>
                            Contact
                        </NextLink>
                    </Stack>
                </Flex>
            </Box>
            <Box as="main" flex="1" padding="4" maxWidth="1200px" mx="auto">
                {children}
            </Box>
            <Box
                as="footer"
                bg="blue.500"
                color="white"
                padding="4"
                textAlign="center"
            >
                © 2024 Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Donec a diam lectus. Sed sit amet ipsum mauris. Maecenas congue
            </Box>
        </Flex>
    )
}

export default Layout
