import { Button as ChakraButton } from '@chakra-ui/react';

interface ButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  children: React.ReactNode;
}

export const Button = ({ children, ...rest }: ButtonProps) => {
  return <ChakraButton {...rest}>{children}</ChakraButton>;
};
